<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/*********************************************************************************************
* *
* * //// RUTAS PARA LA GETION DE LOS TOKENS DE ACCESO Y SEGURIDAD \\\
* *
**********************************************************************************************/

Route::post('iniciar-sesion', 'ApiController@login');
Route::post('registrar', 'ApiController@signup');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('cerrar-sesion', 'ApiController@logout');
    // Route::get('user', 'ApiController@user');
    Route::get('products', 'ApiController@getProducts');
    Route::get('order/{order?}', 'ApiController@getOrder');
});
