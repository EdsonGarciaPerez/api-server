<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    public function items()
    {
        return $this->belongsToMany('App\Models\Product', 'order_items', 'order_id', 'product_id')->withPivot('quantity', 'price');
    }
}
