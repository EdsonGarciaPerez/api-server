<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use DB;

//Models
use App\User;
use App\Models\Product;
use App\Models\Order;


class ApiRepository
{

    public function registerUser($data)
    {
        $user = new User([
            'name'     => $data->name,
            'email'    => $data->email,
            'password' => bcrypt($data->password),
        ]);
        $user->save();
    }
    
    public function makeTokenApi($data)
    {
        $user = $data->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($data->remember_me) {
            $token->expires_at = Carbon::now("America/Mexico_City")->addHour();
        }
        $token->save();

        return $tokenResult;
    }

    public function getAllProducts()
    {
        return Product::all();
    }
    
    public function getOrderByNumber($order)
    {
        return Order::where("number", $order)->first();
    }
}