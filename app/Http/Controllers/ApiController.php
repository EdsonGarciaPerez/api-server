<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
//Repositories
use App\Repositories\ApiRepository;

class ApiController extends Controller
{
    private $apiRepository;

    public function __construct(
        ApiRepository $apiRepository
    ){
        $this->apiRepository = $apiRepository;
    }

    public function signup(Request $request){
        $validator = Validator::make($request->all(), [
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Data insertion failed']);
        }
        
        $this->apiRepository->registerUser($request);
        
        return response()->json([
            'message' => 'Successfully created user!'], 201);
    }
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Data insertion failed']);
        }

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'], 401);
        }

        $token = $this->apiRepository->makeTokenApi($request);
        
        return response()->json([
            'access_token' => $token->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $token->token->expires_at)
                    ->toDateTimeString(),
        ]);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json(['message' => 
            'Successfully logged out']);
    }

    // public function user(Request $request)
    // {
    //     return response()->json($request->user());
    // }

    /*********************************************************************************************
    * *
    * * //// METODOS PARA LA OBTENCION DE DATOS
    * *
    **********************************************************************************************/
    
    public function getProducts(Request $request){
       $products = $this->apiRepository->getAllProducts();
        return response()->json($products);
    }

    public function getOrder($order = null){
        if(!$order){
            return response()->json([
                'message' => 'No Found!!'], 401);
        }
        $order = $this->apiRepository->getOrderByNumber($order);
        $order->load("items");
        return response()->json($order);
    }

}
